package com.example.climb.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ClimbGrade {

  @Column(name = "climb_sn")
  private Long climbSn;

  @Column(name = "grade")
  private String grade;

  @Column(name = "sn")
  private Integer sn;

  @Column(name = "note")
  private String note;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Column(name = "orgn_file_nm")
  private String orgnFileNm;

  @Column(name = "file_path")
  private String filePath;
}
