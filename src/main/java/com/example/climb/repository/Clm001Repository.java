package com.example.climb.repository;

import com.example.climb.jpo.Clm001;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface Clm001Repository extends JpaRepository<Clm001, Long> {
  @Transactional
  @Modifying
  @Query("DELETE FROM Clm001 c WHERE c.climbSn = :climbSn")
  void deleteAllByClimbSn(Long climbSn);
}
