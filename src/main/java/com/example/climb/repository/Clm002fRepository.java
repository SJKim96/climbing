package com.example.climb.repository;

import com.example.climb.jpo.Clm002f;
import com.example.climb.jpo.ClimbDtl;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Clm002fRepository extends JpaRepository<Clm002f, Long> {
  List<Clm002f> findAllByClimbSn(Long climbSn);
  Clm002f findByClimbSnAndGradeAndSn(Long climbSn, String grade, Integer sn);

  @Transactional
  @Modifying
  @Query("DELETE FROM Clm002f c WHERE c.climbSn = :climbSn AND c.grade = :grade AND c.sn = :sn")
  void deleteByClimbSnAndGradeAndSn(Long climbSn, String grade, Integer sn);

}
