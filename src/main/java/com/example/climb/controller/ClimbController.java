package com.example.climb.controller;

import com.example.climb.jpo.*;
import com.example.climb.service.ClimbService;
import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.swing.text.html.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ClimbController {
  private static final Logger LOG = LoggerFactory.getLogger(ClimbController.class);
  @Resource(name = "ClimbService")
  private ClimbService climbService;

  public static class API {

    public static final String BASE = "/api/climb";

    // 클라이밍장 조회
    public static final String GET_CLIMB_LIST = BASE + "/clmib-list";
    // 클라이밍장 등록
    public static final String POST_CLIMB = BASE + "/climb/{climbNm}";
    // 클라이밍장 삭제
    public static final String DELETE_CLIMB = BASE + "/climb/{climbSn}";
    // 클라이밍장 수정
    public static final String PATCH_CLIMB = BASE + "/climb/{climbSn}/{climbNm}";
    // 클라이밍장별 난이도 조회
    public static final String GET_CLIMB_GRADE_LIST = BASE + "/climb-grade-list/{climbSn}";
    // 클라이밍장별 난이도 등록
    public static final String POST_CLIMB_GRADE = BASE + "/climb-grade/{climbSn}/{grade}";
    // 클라이밍장별 난이도 수정
    public static final String PATCH_CLIMB_GRADE = BASE + "/climb-grade/{climbSn}/{grade}/{sn}";
    // 클라이밍장별 난이도 삭제
    public static final String DELETE_CLIMB_GRADE = BASE + "/climb-grade/{climbSn}/{grade}/{sn}";
  }

  //클라이밍장 조회
  @GetMapping(API.GET_CLIMB_LIST)
  public ResponseEntity<?> getsClimbList() {
    Map<String, Object> responseBody = new HashMap<>();

    responseBody = climbService.selectClimbList();

    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 등록
  @PostMapping(API.POST_CLIMB)
  public ResponseEntity<?> postClimb(
    @PathVariable Optional<String> climbNm
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    if(climbNm.isPresent())
      responseBody = climbService.saveClimb(climbNm.get());
    else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 삭제
  @DeleteMapping(API.DELETE_CLIMB)
  public ResponseEntity<?> deleteClimb(
    @PathVariable Optional<Long> climbSn
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    if(climbSn.isPresent())
      responseBody = climbService.deleteClimb(climbSn.get());
    else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 수정
  @PatchMapping(API.PATCH_CLIMB)
  public ResponseEntity<?> patchClimb(
      @PathVariable Optional<Long> climbSn,
      @PathVariable Optional<String> climbNm
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    if(climbSn.isPresent() && climbNm.isPresent())
      responseBody = climbService.updateClimb(climbSn.get(), climbNm.get());
    else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 난이도 수정
  @GetMapping(API.GET_CLIMB_GRADE_LIST)
  public ResponseEntity<?> getsClimbGradeList(
    @PathVariable Optional<Long> climbSn
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    if(climbSn.isPresent())
      responseBody = climbService.selectClimbGradeList(climbSn.get());
    else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 난이도 등록
  @PostMapping(API.POST_CLIMB_GRADE)
  public ResponseEntity<?> postClimbGrade(
    @PathVariable Optional<Long> climbSn,
    @PathVariable Optional<String> grade,
    @RequestParam(required = false) String note,
    @RequestPart(required = false) MultipartFile uploadFiles
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    Clm002 clm002 = new Clm002();
    if (climbSn.isPresent() && grade.isPresent()) {
      clm002 = clm002.builder().grade(grade.get()).note(note).build();
      clm002.setClimbSn(climbSn.get());

      responseBody = climbService.saveClimbGrade(clm002, uploadFiles);
    } else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 난이도 수정
  @PostMapping(API.PATCH_CLIMB_GRADE)
  public ResponseEntity<?> patchClimbGrade(
      @PathVariable Optional<Long> climbSn,
      @PathVariable Optional<String> grade,
      @PathVariable Optional<Integer> sn,
      @RequestParam(required = false) String note,
      @RequestParam(required = false, defaultValue = "N") String deleteYn,
      @RequestPart(required = false) MultipartFile uploadFiles
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    Clm002 clm002 = new Clm002();
    if (climbSn.isPresent() && grade.isPresent() && sn.isPresent()) {
      clm002 = clm002.builder().grade(grade.get()).note(note).build();
      clm002.setClimbSn(climbSn.get());
      clm002.setSn(sn.get());

      responseBody = climbService.updateClimbGrade(clm002, deleteYn, uploadFiles);
    } else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }

  //클라이밍장 난이도 삭제
  @DeleteMapping(API.DELETE_CLIMB_GRADE)
  public ResponseEntity<?> deleteClimbGrade(
      @PathVariable Optional<Long> climbSn,
      @PathVariable Optional<String> grade,
      @PathVariable Optional<Integer> sn
  ) {
    Map<String, Object> responseBody = new HashMap<>();
    Clm002 clm002 = new Clm002();
    if (climbSn.isPresent() && grade.isPresent() && sn.isPresent()) {
      clm002 = clm002.builder().grade(grade.get()).build();
      clm002.setClimbSn(climbSn.get());
      clm002.setSn(sn.get());

      responseBody = climbService.deleteClimbGrade(clm002);
    } else {
      responseBody.put("code", 203); // 파라미터 조건 미충족
    }
    return ResponseEntity.ok(responseBody);
  }
}
