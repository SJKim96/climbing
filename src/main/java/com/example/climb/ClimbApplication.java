package com.example.climb;

import com.example.climb.jpo.Clm001;
import com.example.climb.service.ClimbService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClimbApplication {

  public static void main(String[] args) {
    SpringApplication.run(ClimbApplication.class, args);
  }

}
