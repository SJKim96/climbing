package com.example.climb.jpo;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.Builder;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "clm002")
@IdClass(ClimbDtl.class)
public class Clm002 implements Serializable {

  @Id
  @Column(name = "climb_sn")
  private Long climbSn;

  @Id
  @Column(name = "grade")
  private String grade;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "sn")
  private Integer sn;

  @Column(name = "note")
  private String note;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Builder
  public Clm002(String grade, String note) {
    this.grade = grade;
    this.note = note;
    this.inptDt = LocalDate.now();
  }
}
