package com.example.climb.jpo;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClimbDtl implements Serializable {
  private long climbSn;
  private String grade;
  private Integer sn;

}
