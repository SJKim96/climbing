package com.example.climb.jpo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "clm002f")
public class Clm002f implements Serializable {

  @Id
  @Column(name = "climb_sn")
  private Long climbSn;

  @Column(name = "grade")
  private String grade;

  @Column(name = "sn")
  private Integer sn;

  @Column(name = "orgn_file_nm")
  private String orgnFileNm;

  @Column(name = "file_path")
  private String filePath;

  @Column(name = "inpt_dt")
  private LocalDate inptDt;

  @Builder
  public Clm002f(String grade) {
    this.grade = grade;
    this.inptDt = LocalDate.now();
  }
}
