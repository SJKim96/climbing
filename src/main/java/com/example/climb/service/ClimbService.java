package com.example.climb.service;

import com.example.climb.jpo.*;
import com.example.climb.dto.*;
import com.example.climb.repository.*;
import com.example.climb.utils.FileUploadData;
import com.example.climb.utils.FileUploadType;
import com.example.climb.utils.FileUploader;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

@Service("ClimbService")
public class ClimbService {
  @Autowired
  private Clm001Repository clm001Repository;

  @Autowired
  private Clm002Repository clm002Repository;

  @Autowired
  private Clm002fRepository clm002fRepository;

  // 클라이밍장 조회
  public Map<String, Object> selectClimbList() {
    Map<String, Object> result = new HashMap<>();
    List<Clm001> climbList = new ArrayList<>();
    int code = 999;
    try {
      climbList = clm001Repository.findAll();
      code = 200;
    } catch (Exception e) {
      code = 999;
    } finally {
      result.put("result", climbList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 등록
  public Map<String, Object> saveClimb(String climbNm) {
    Map<String, Object> result = new HashMap<>();
    int code = 999;
    try {
      Clm001 climbDtl = Clm001.builder().climbNm(climbNm).build();
      clm001Repository.save(climbDtl);
      code = 200;
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 삭제
  public Map<String, Object> deleteClimb(Long climbSn) {
    Map<String, Object> result = new HashMap<>();
    List<Clm002f> climbFileList = new ArrayList<>();
    int code = 999;
    try {
      climbFileList = clm002fRepository.findAllByClimbSn(climbSn);
      for (Clm002f file : climbFileList) {
        File filePath = new File(file.getFilePath() + file.getOrgnFileNm());
        if (filePath.exists() && filePath.isFile()) {
          if (filePath.delete()) {
            clm002fRepository.deleteByClimbSnAndGradeAndSn(file.getClimbSn(), file.getGrade(), file.getSn());
          } else {
            result.put("code", 202);
          }
        }
      }
      clm002Repository.deleteAllByClimbSn(climbSn);
      clm001Repository.deleteAllByClimbSn(climbSn);
      code = 200;
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 202;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 별 난이도 조회
  public Map<String, Object> selectClimbGradeList(Long climbSn) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    int code = 999;
    try {
      climbGradeList = clm002Repository.selectClimbGradeList(climbSn);
      code = 200;
    } catch (Exception e) {
      code = 999;
    } finally {
      result.put("result", climbGradeList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 난이도 등록
  public Map<String, Object> saveClimbGrade(Clm002 clm002, MultipartFile uploadFiles) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    List<Map<String, Object>> failedList = new ArrayList<>();
    List<String> imgAllowExtensions = new ArrayList<>(Arrays.asList("jpeg", "jpg", "png", "gif"));
    int code = 999;
    try {
      Clm002 climbGrade = clm002Repository.save(clm002);
      if (climbGrade != null) {
        if (uploadFiles != null) {
          // 파일 등록
          FileUploader uploader1 = new FileUploader(uploadFiles, imgAllowExtensions);
          uploader1.setEvents(new FileUploader.Events() {
            @Override
            public boolean onStoreDatabase(FileUploadData uploadData) {
              boolean isStored = false;
              try {
                Clm002f fileData = Clm002f.builder().grade(climbGrade.getGrade()).build();
                fileData.setClimbSn(climbGrade.getClimbSn());
                fileData.setSn(climbGrade.getSn());
                fileData.setOrgnFileNm(uploadData.getOriginalFileName());
                fileData.setFilePath(uploadData.getSaveFilePath());

                if (clm002fRepository.save(fileData) != null) { // 파일 테이블 insert
                  isStored = true;
                }
              } catch (DataAccessException e) {
                // Error
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
              }

              return isStored;
            }

            @Override
            public void onSuccess(FileUploadData successData) {
              // onSuccess
            }

            @Override
            public void onError(FileUploadData errorData) {
              // onError
            }

            @Override
            public void onFinish(List<FileUploadData> successList, List<FileUploadData> errorList,
                FileUploadType type, boolean isSuccessAll) {
              // onFinish
              errorList.forEach(fileUploadData -> {
                Map<String, Object> data = new HashMap<>();
                data.put("fileName", fileUploadData.getOriginalFileName());
                data.put("mimeType", fileUploadData.getMimeType());
                data.put("fileSize", fileUploadData.getSize());
                data.put("message", fileUploadData.getType().getMessage());
                failedList.add(data);
              });
            }
          });

          switch (uploader1.uploadAll(new File("C:\\sjKimProject\\climb\\src\\main"))) {
            case SUCCESS:
              code = 200;
              break;
            case FAILED:
              code = 208; // 파일 업로드 실패
              break;
            default:
              code = 999;
              break;
          }
        }
      }
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 999;
    } finally {
      result.put("result", climbGradeList);
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 이름 변경
  public Map<String, Object> updateClimb(Long climbSn, String climbNm) {
    Map<String, Object> result = new HashMap<>();
    int code = 999;
    try {
      Optional<Clm001> climbDtl = clm001Repository.findById(climbSn);
      if (climbDtl.isPresent()) {
        climbDtl.get().setClimbNm(climbNm);
        clm001Repository.save(climbDtl.get());
        code = 200;
      }
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("code", code);
    }
    return result;
  }

  //클라이밍장 난이도 수정
  public Map<String, Object> updateClimbGrade(Clm002 clm002, String deleteYn, MultipartFile uploadFiles) {
    Map<String, Object> result = new HashMap<>();
    List<ClimbGrade> climbGradeList = new ArrayList<>();
    List<Map<String, Object>> failedList = new ArrayList<>();
    Clm002 climbGrade = new Clm002();

    List<String> imgAllowExtensions = new ArrayList<>(Arrays.asList("jpeg", "jpg", "png", "gif"));
    int code = 999;
    try {
      if (uploadFiles != null || deleteYn.equals("Y")) {
        Clm002f climbFile = clm002fRepository.findByClimbSnAndGradeAndSn(clm002.getClimbSn(), clm002.getGrade(), clm002.getSn());
        if (climbFile != null) {
          File filePath = new File(climbFile.getFilePath() + climbFile.getOrgnFileNm());
          if (filePath.exists() && filePath.isFile()) {
            if (filePath.delete()) {
              clm002fRepository.deleteByClimbSnAndGradeAndSn(clm002.getClimbSn(), clm002.getGrade(),
                  clm002.getSn());
            } else {
              result.put("code", 202);
            }
          }
        }
      }
      climbGrade = clm002Repository.save(clm002);
      if (climbGrade != null) {
        if (uploadFiles != null) {
          // 파일 등록
          FileUploader uploader1 = new FileUploader(uploadFiles, imgAllowExtensions);
          Clm002 finalClimbGrade = climbGrade;
          uploader1.setEvents(new FileUploader.Events() {
            @Override
            public boolean onStoreDatabase(FileUploadData uploadData) {
              boolean isStored = false;
              try {
                Clm002f fileData = Clm002f.builder().grade(finalClimbGrade.getGrade()).build();
                fileData.setClimbSn(finalClimbGrade.getClimbSn());
                fileData.setSn(finalClimbGrade.getSn());
                fileData.setOrgnFileNm(uploadData.getOriginalFileName());
                fileData.setFilePath(uploadData.getSaveFilePath());

                if (clm002fRepository.save(fileData) != null) { // 파일 테이블 insert
                  isStored = true;
                }
              } catch (DataAccessException e) {
                // Error
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
              }

              return isStored;
            }

            @Override
            public void onSuccess(FileUploadData successData) {
              // onSuccess
            }

            @Override
            public void onError(FileUploadData errorData) {
              // onError
            }

            @Override
            public void onFinish(List<FileUploadData> successList, List<FileUploadData> errorList,
                FileUploadType type, boolean isSuccessAll) {
              // onFinish
              errorList.forEach(fileUploadData -> {
                Map<String, Object> data = new HashMap<>();
                data.put("fileName", fileUploadData.getOriginalFileName());
                data.put("mimeType", fileUploadData.getMimeType());
                data.put("fileSize", fileUploadData.getSize());
                data.put("message", fileUploadData.getType().getMessage());
                failedList.add(data);
              });
            }
          });

          switch (uploader1.uploadAll(new File("C:\\sjKimProject\\climb\\src\\main"))) {
            case SUCCESS:
              code = 200;
              break;
            case FAILED:
              code = 208; // 파일 업로드 실패
              break;
            default:
              code = 999;
              break;
          }
        }
      }
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 201;
    } finally {
      result.put("result", climbGrade);
      result.put("code", code);
    }
    return result;
  }

  // 클라이밍장 난이도 삭제
  public Map<String, Object> deleteClimbGrade(Clm002 clm002) {
    Map<String, Object> result = new HashMap<>();

    int code = 999;
    try {
      Clm002f climbFile = clm002fRepository.findByClimbSnAndGradeAndSn(clm002.getClimbSn(), clm002.getGrade(), clm002.getSn());
      if (climbFile != null) {
        File filePath = new File(climbFile.getFilePath() + climbFile.getOrgnFileNm());
        if (filePath.exists() && filePath.isFile()) {
          if (filePath.delete()) {
            clm002fRepository.deleteByClimbSnAndGradeAndSn(clm002.getClimbSn(), clm002.getGrade(), clm002.getSn());
          } else {
            result.put("code", 202);
          }
        }
      }
      clm002Repository.deleteByClimbSnAndGradeAndSn(clm002.getClimbSn(), clm002.getGrade(), clm002.getSn());
    } catch (Exception e) {
      TransactionAspectSupport.currentTransactionStatus().setRollbackOnly(); // Rollback
      code = 202;
    } finally {
      result.put("code", code);
    }
    return result;
  }
}
