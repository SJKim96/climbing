package com.example.climb.utils;

public enum FileUploadType {
  UNKNOWN(0, "알 수 없는 오류가 발생했습니다."),
  SUCCESS(1, "업로드에 성공했습니다."),
  FAILED(2, "업로드에 실패했습니다."),
  IDLE(3, "유휴 상태입니다."),
  DIR_NOTFOUND(4, "해당 경로를 찾을 수 없습니다."),
  INTEGRITY_FAILD(5, "업로드 중 파일이 손상되었습니다."),
  DB_STORE_FAILD(6, "업로드 중 데이터베이스 저장에 실패했습니다."),
  MIME_TYPE(7, "해당 파일 형식은 사용할 수 없습니다."),
  EXTENSION(8, "해당 파일 확장자는 사용할 수 없습니다."),
  FILE_SIZE(9, "파일의 크기가 너무 큽니다."),
  IMG_SIZE(10, "이미지의 크기가 너무 큽니다.");

  private int code;
  private String message;

  private FileUploadType(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
