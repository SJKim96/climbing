package com.example.climb.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName : FileUploader.java
 * @Description : 파일 업로드 및 DB 적재 관리 객체
 * @author : 이주원
 * @since : 2023. 02. 08.
 */
public class FileUploader {
  private static final long DEFAULT_FILE_SIZE = 10485760; // 10MB(1,048,576 * 10)
  private FileUploadType responseType = FileUploadType.UNKNOWN;
  private Events events; // Callback Events
  private List<MultipartFile> fileItems;
  private List<String> mimeTypes; // 허용 MimeType
  private List<String> extensions; // 허용 확장자
  private long fileSize; // 허용 파일크기

  // @FunctionalInterface
  public interface Events {
    boolean onStoreDatabase(FileUploadData uploadData);
    void onSuccess(FileUploadData successData);
    void onError(FileUploadData errorData);
    void onFinish(List<FileUploadData> successList, List<FileUploadData> errorList, FileUploadType fileUploadType, boolean isSuccessAll);
  }

  public FileUploader(MultipartFile multipartFile, List<String> extensions) {
    this(multipartFile, extensions, new ArrayList<>(), DEFAULT_FILE_SIZE);
  }

  public FileUploader(MultipartFile multipartFile, List<String> extensions, List<String> mimeTypes) {
    this(multipartFile, extensions, mimeTypes, DEFAULT_FILE_SIZE);
  }

  public FileUploader(MultipartFile multipartFile, List<String> extensions, long fileSize) {
    this(multipartFile, extensions, new ArrayList<>(), fileSize);
  }

  public FileUploader(MultipartFile multipartFile, List<String> extensions, List<String> mimeTypes, long fileSize) {
    this(new ArrayList<>(Arrays.asList(multipartFile)), extensions, mimeTypes, fileSize);
  }

  public FileUploader(List<MultipartFile> multipartFiles, List<String> extensions) {
    this(multipartFiles, extensions, new ArrayList<>(), DEFAULT_FILE_SIZE);
  }

  public FileUploader(List<MultipartFile> multipartFiles, List<String> extensions, List<String> mimeTypes) {
    this(multipartFiles, extensions, mimeTypes, DEFAULT_FILE_SIZE);
  }
  public FileUploader(List<MultipartFile> multipartFiles, List<String> extensions, long fileSize) {
    this(multipartFiles, extensions, new ArrayList<>(), fileSize);
  }

  public FileUploader(List<MultipartFile> multipartFiles, List<String> extensions, List<String> mimeTypes, long fileSize) {
    this.fileItems = new ArrayList<>(multipartFiles);
    this.extensions = extensions;
    this.mimeTypes = mimeTypes;
    this.fileSize = fileSize;
  }

  public FileUploadType getFileUploadType() {
    return responseType;
  }

  private void setFileUploadType(FileUploadType responseType) {
    this.responseType = responseType;
  }

  public List<String> getExtensions() {
    return extensions;
  }

  public List<String> getMimeTypes() {
    return mimeTypes;
  }

  public long getFileSize() {
    return fileSize;
  }

  public void setEvents(Events events) {
    this.events = events;
  }

  protected boolean doFilter(FileUploadData uploadData) {
    boolean hasExtensions = (extensions.isEmpty() ? true : extensions.stream().anyMatch(extension -> uploadData.getExtension().equalsIgnoreCase(extension)));
    boolean hasMimeType = (mimeTypes.isEmpty() ? true : mimeTypes.stream().anyMatch(mimeType -> uploadData.getMimeType().equalsIgnoreCase(mimeType)));
    boolean isAllowSize = uploadData.getSize() <= fileSize;

    if (!hasExtensions) uploadData.setType(FileUploadType.EXTENSION);
    else if (!hasMimeType) uploadData.setType(FileUploadType.MIME_TYPE);
    else if (!isAllowSize) uploadData.setType(FileUploadType.FILE_SIZE);

    return (hasExtensions && hasMimeType && isAllowSize);
  }

  private FileUploadData upload(MultipartFile multipartFile, File prefix) {
    String saveFileName = multipartFile.getOriginalFilename();
    String saveFilePath = prefix.getPath();

    if (saveFilePath.charAt(saveFilePath.length() - 1) == File.separatorChar) saveFilePath = saveFilePath.substring(0, saveFilePath.length() - 2);
    saveFilePath += getTimeBasePath();

    FileUploadData uploadData = null;
    try {
      boolean isIntegrity = false;
      uploadData = new FileUploadData(multipartFile, saveFileName, saveFilePath);

      if (doFilter(uploadData)) { // 파일 형식 & 확장자 & 크기 필터
        File dir = new File(saveFilePath);
        if (!dir.isDirectory()) { // 폴더가 없다면 생성
          if (!dir.mkdirs()) { // 폴더 생성
            uploadData.setType(FileUploadType.DIR_NOTFOUND);
          }
        }

        String fileHash = getSHA256(multipartFile.getBytes());
        File uploadPath = new File(saveFilePath + File.separator + saveFileName);
        multipartFile.transferTo(uploadPath);

        uploadData.setType(FileUploadType.DIR_NOTFOUND);
        if (uploadPath.exists()) {
          if (getSHA256(Files.readAllBytes(uploadPath.toPath())).equals(fileHash)) {
            uploadData.setType(FileUploadType.IDLE);
            isIntegrity = true; // 무결성 확인 완료
          } else {
            uploadData.setType(FileUploadType.INTEGRITY_FAILD);
            uploadPath.delete(); // 손상된 파일 삭제
          }
        }

        if (isIntegrity && uploadData.getType() == FileUploadType.IDLE) { // 무결성 체크 완료
          boolean isStored = this.events.onStoreDatabase(uploadData);
          if (isStored) {
            uploadData.setType(FileUploadType.SUCCESS);
            uploadData.setIsUploaded(true);
          } else {
            uploadData.setType(FileUploadType.DB_STORE_FAILD);
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
      uploadData.setType(FileUploadType.UNKNOWN);
    }

    return uploadData;
  }

  public FileUploadType uploadAll(File path) {
    List<FileUploadData> successList = new ArrayList<>();
    List<FileUploadData> errorList = new ArrayList<>();
    int fileCount = this.fileItems.size();

    // 전체 결과 목록
    this.fileItems.forEach(fileItem -> {
      final FileUploadData uploadData = this.upload(fileItem, path);
      if (uploadData.isUploaded()) {
        successList.add(uploadData);
        this.events.onSuccess(uploadData);
      } else {
        errorList.add(uploadData);
        this.events.onError(uploadData);
      }
    });

    boolean isSuccessAll = (successList.size() == fileCount);
    if (isSuccessAll) this.setFileUploadType(FileUploadType.SUCCESS);
    else this.setFileUploadType(FileUploadType.FAILED);

    this.events.onFinish(successList, errorList, responseType, isSuccessAll);

    return responseType;
  }

  public static String getTimeBasePath() {
    StringBuilder stringBuilder = new StringBuilder(File.separator);

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int day = calendar.get(Calendar.DATE);
    int hour = calendar.get(Calendar.HOUR);
    int minute = calendar.get(Calendar.MINUTE);

    stringBuilder.append(year);
    stringBuilder.append(File.separator);
    stringBuilder.append(String.format("%02d", month));
    stringBuilder.append(File.separator);
    stringBuilder.append(String.format("%02d", day));
    stringBuilder.append(File.separator);
    stringBuilder.append(String.format("%02d", hour));
    stringBuilder.append(String.format("%02d", minute));
    stringBuilder.append(File.separator);

    return stringBuilder.toString();
  }

  public static String getSHA256(byte[] binary) {
    String hash = "";
    try {
      MessageDigest sh = MessageDigest.getInstance("SHA-256");
      sh.update(binary);

      byte byteData[] = sh.digest();
      StringBuffer sb = new StringBuffer();
      for(int i = 0 ; i < byteData.length ; i++) sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));

      hash = sb.toString();
    } catch(NoSuchAlgorithmException e) {
      hash = "";
    }

    return hash;
  }
}
