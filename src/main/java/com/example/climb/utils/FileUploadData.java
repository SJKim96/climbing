package com.example.climb.utils;

import java.io.IOException;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName : FileUploadData.java
 * @Description : 파일 업로드 및 DB 적재 결과 객체
 * @author : 이주원
 * @since : 2023. 02. 08.
 */
public class FileUploadData {
  private String mappedName;
  private String originalFileName;
  private String saveFileName;
  private String saveFilePath;
  private String mimeType;
  private String binaryData;
  private long size;
  private boolean isUploaded;
  private FileUploadType type;

  @SneakyThrows
  public FileUploadData(MultipartFile multipartFile) throws IOException {
    this(multipartFile.getName(), multipartFile.getOriginalFilename(), "", "", multipartFile.getBytes(), multipartFile.getSize(), false);
  }

  @SneakyThrows
  public FileUploadData(MultipartFile multipartFile, String saveFileName, String saveFilePath) throws IOException {
    this(multipartFile.getName(), multipartFile.getOriginalFilename(), saveFileName, saveFilePath, multipartFile.getBytes(), multipartFile.getSize(), false);
  }

  @SneakyThrows
  public FileUploadData(String mappedName, String originalFileName, String saveFileName, String saveFilePath, byte[] binaryData, long size, boolean isUploaded) {
    Tika tika = new Tika();
    // this.originalFileName = URLEncoder.encode(originalFileName, "UTF-8");
    // this.saveFileName = URLEncoder.encode(saveFileName, "UTF-8");
    // this.saveFilePath = URLEncoder.encode(saveFilePath, "UTF-8");
    this.mappedName = mappedName;
    this.originalFileName = originalFileName;
    this.saveFileName = saveFileName;
    this.saveFilePath = saveFilePath;
    // this.mimeType = new MimetypesFileTypeMap().getContentType(originalFileName.toLowerCase());
    this.mimeType = tika.detect(originalFileName).toLowerCase();
    this.binaryData = new String(Base64.encodeBase64(binaryData));
    this.size = size;
    this.isUploaded = isUploaded;
    this.type = FileUploadType.IDLE;
  }

  @SneakyThrows
  public String getMappedName() {
    return mappedName;
  }

  @SneakyThrows
  public String getOriginalFileName() {
    return originalFileName;
  }

  @SneakyThrows
  public void setOriginalFileName(String originalFileName) {
    Tika tika = new Tika();
    this.originalFileName = originalFileName;
    // this.mimeType = new MimetypesFileTypeMap().getContentType(originalFileName).toLowerCase();
    this.mimeType = tika.detect(originalFileName).toLowerCase();
  }

  @SneakyThrows
  public String getSaveFileName() {
    return saveFileName;
  }

  @SneakyThrows
  public String getSaveFilePath() {
    return saveFilePath;
  }

  public String getExtension() {
    return FilenameUtils.getExtension(originalFileName).toLowerCase();
  }

  public String getMimeType() {
    return mimeType;
  }

  public byte[] getBinaryData() {
    return Base64.decodeBase64(binaryData);
  }

  public void setBinaryData(byte[] binaryData) {
    this.binaryData = new String(Base64.encodeBase64(binaryData));
  }

  public long getSize() { return size; }

  public void setSize(long size) { this.size = size; }

  public boolean isUploaded() { return isUploaded; }

  public void setIsUploaded(boolean isUploaded) { this.isUploaded = isUploaded; }

  public FileUploadType getType() {
    return type;
  }

  public void setType(FileUploadType type) {
    this.type = type;
  }

}
